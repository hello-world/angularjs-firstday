var app = angular.module('app', ['app.controllers','ngRoute']);
app.controllers = angular.module('app.controllers', []);

// ----------------- Add Router ----------------
app.config(['$routeProvider',function ($routeProvider) {

    $routeProvider
        .when('/users/:index', {
            controller: 'UserCtrl',
            templateUrl: 'html/user.html'
        }).when('/users', {
            controller: 'MainCtrl',
            templateUrl: 'html/main.html'
        });
    $routeProvider.otherwise({
            redirectTo: '/users'
        });
}]);